//
//  PastViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 16/07/21.
//

import UIKit

class PastViewController: UIViewController {

    let textLabel: UILabel = UILabel(frame: CGRect(x: 16, y: 8, width: 100, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.text = "Empty here"
        textLabel.textColor = .black
        
        self.view.addSubview(textLabel)
    }

}
