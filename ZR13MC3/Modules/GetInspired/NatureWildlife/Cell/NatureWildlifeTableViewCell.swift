//
//  NatureWildlifeTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

class NatureWildlifeTableViewCell: UITableViewCell {
    
    static let identifier = "NatureWildlifeTableViewCell"
    
    private let natureWildlifeImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let natureWildlifeNameLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.boldSystemFont(ofSize: 13)
        return titleLabel
    }()
    
    private let natureWildlifeAddressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        return addressLabel
    }()
    
    private let natureWildlifeTimeLabel: UILabel = {
        let timeLabel = UILabel()
        timeLabel.font = UIFont.boldSystemFont(ofSize: 12)
        return timeLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(natureWildlifeImageView)
        contentView.addSubview(natureWildlifeNameLabel)
        contentView.addSubview(natureWildlifeAddressLabel)
        contentView.addSubview(natureWildlifeTimeLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(image: String, name: String, address: String, time: String) {
        natureWildlifeImageView.image = UIImage(named: image)
        natureWildlifeNameLabel.text = name
        natureWildlifeAddressLabel.text = address
        natureWildlifeTimeLabel.text = time
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        natureWildlifeImageView.image = nil
        natureWildlifeNameLabel.text = nil
        natureWildlifeAddressLabel.text = nil
        natureWildlifeTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        natureWildlifeImageView.frame = CGRect(x: 14,
                                        y: 18,
                                        width: 85,
                                        height: contentView.frame.size.height - 36)
        
        natureWildlifeNameLabel.frame = CGRect(x: 28 + natureWildlifeImageView.frame.size.width,
                                         y: 18,
                                         width: contentView.frame.size.width - natureWildlifeImageView.frame.size.width - 10,
                                         height: 21)
                                            
        natureWildlifeAddressLabel.frame = CGRect(x: natureWildlifeImageView.frame.size.width + 28,
                                                  y: natureWildlifeNameLabel.frame.height + 16,
                                                  width: contentView.frame.size.width - natureWildlifeImageView.frame.size.width - 8,
                                                  height: natureWildlifeNameLabel.frame.size.height + 23)
        
        natureWildlifeTimeLabel.frame = CGRect(x: natureWildlifeImageView.frame.size.width + 28,
                                           y: natureWildlifeNameLabel.frame.height + natureWildlifeAddressLabel.frame.size.height - 10,
                                           width: contentView.frame.size.width - natureWildlifeImageView.frame.size.width - 8,
                                           height: natureWildlifeNameLabel.frame.size.height + natureWildlifeAddressLabel.frame.size.height)
    }
}
