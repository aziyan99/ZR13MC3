//
//  EventViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

struct EventDatas {
    let image: String
    let name: String
    let address: String
    let time: String
}

class EventViewController: UIViewController {
    
    let events: [EventDatas] = [
        EventDatas(image: "imgtes1", name: "Tour de Bintan", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 2", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 3", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 4", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 5", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 6", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 7", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 8", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 9", address: "Di hatimu", time: "09.00 - 17.00"),
        EventDatas(image: "rect", name: "Event 10", address: "Di hatimu", time: "09.00 - 17.00"),
    ]
    
    private let eventTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(EventTableViewCell.self, forCellReuseIdentifier: EventTableViewCell.identifier)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Upcoming Events"
        eventTableView.dataSource = self
        eventTableView.delegate = self
        
        view.addSubview(eventTableView)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        eventTableView.frame = view.bounds
    }
}

extension EventViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = eventTableView.dequeueReusableCell(withIdentifier: EventTableViewCell.identifier, for: indexPath) as? EventTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(image: events[indexPath.row].image, name: events[indexPath.row].name, address: events[indexPath.row].address, time: events[indexPath.row].time)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventTableView.deselectRow(at: indexPath, animated: true)
        
        let eventDetailsStoryboard = UIStoryboard(name: "EventDetails", bundle: nil)
        let eventDetailsViewController = eventDetailsStoryboard.instantiateViewController(identifier: "EventDetailsViewController") as! EventDetailsViewController
        eventDetailsViewController.titleText = events[indexPath.row].name
        eventDetailsViewController.detailData = events[indexPath.row]
        let navController = UINavigationController(rootViewController: eventDetailsViewController)
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
    

}
