//
//  OnboardingCollectionViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 16/07/21.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var slideTitleLabel: UILabel!
    @IBOutlet weak var slideImageView: UIImageView!
    @IBOutlet weak var slideDescriptionLabel: UILabel!
    
    func setup(_ slide: OnboardingSlide) {
        slideTitleLabel.text = slide.title
        slideImageView.image = slide.image
        slideDescriptionLabel.text = slide.description
    }
    
}
