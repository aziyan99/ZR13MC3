//
//  NatureWildlifeViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

struct NatureWildlifeDatas {
    let image: String
    let name: String
    let address: String
    let time: String
}

class NatureWildlifeViewController: UIViewController {
    
    let naturesWildlifes: [NatureWildlifeDatas] = [
        NatureWildlifeDatas(image: "rect", name: "Mangrove Discovery Tour", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "Day/Night"),
        NatureWildlifeDatas(image: "rect", name: "Gunung Bintan Adventure", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
    ]
    
    private let natureWildlifeTableView: UITableView =  {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(NatureWildlifeTableViewCell.self,
                           forCellReuseIdentifier: NatureWildlifeTableViewCell.identifier)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Nature & Wildlife"
        
        natureWildlifeTableView.dataSource = self
        natureWildlifeTableView.delegate = self
        
        view.addSubview(natureWildlifeTableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        natureWildlifeTableView.frame = view.bounds
    }
}

extension NatureWildlifeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return naturesWildlifes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = natureWildlifeTableView.dequeueReusableCell(withIdentifier: NatureWildlifeTableViewCell.identifier, for: indexPath) as? NatureWildlifeTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(image: naturesWildlifes[indexPath.row].image,
                        name: naturesWildlifes[indexPath.row].name,
                        address: naturesWildlifes[indexPath.row].address,
                        time: naturesWildlifes[indexPath.row].time)
        
        return cell
    }
}

extension NatureWildlifeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        natureWildlifeTableView.deselectRow(at: indexPath, animated: true)
        
        print("you select \(naturesWildlifes[indexPath.row].name)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
}
