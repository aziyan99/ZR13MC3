//
//  ContactViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

struct ContactDatas {
    let name: String
    let address: String
    let image: String
    let time: String
}

class ContactViewController: UIViewController {
    
    let contacts: [ContactDatas] = [
        ContactDatas(name: "satu", address: "dihatimu", image: "rect", time: "19.00 - 17.00"),
        ContactDatas(name: "dua", address: "dihatimu", image: "rect", time: "19.00 - 17.00"),
        ContactDatas(name: "tiga", address: "dihatimu", image: "rect", time: "19.00 - 17.00"),
        ContactDatas(name: "empat", address: "dihatimu", image: "rect", time: "19.00 - 17.00"),
        ContactDatas(name: "lima", address: "dihatimu", image: "rect", time: "19.00 - 17.00"),
    ]
    
    private let contactTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(ContactTableViewCell.self, forCellReuseIdentifier: ContactTableViewCell.identifier)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Useful Numbers"
        
        contactTableView.delegate = self
        contactTableView.dataSource = self
        view.addSubview(contactTableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contactTableView.frame = view.bounds
    }
}

extension ContactViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = contactTableView.dequeueReusableCell(withIdentifier: ContactTableViewCell.identifier, for: indexPath) as? ContactTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(name: contacts[indexPath.row].name, address: contacts[indexPath.row].address, image: contacts[indexPath.row].image, time: contacts[indexPath.row].address)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        contactTableView.deselectRow(at: indexPath, animated: true)
        
        let contactDetailsStoryboard = UIStoryboard(name: "ContactDetails", bundle: nil)
        let contactDetailsViewController = contactDetailsStoryboard.instantiateViewController(identifier: "ContactDetailsViewController") as! ContactDetailsViewController
        contactDetailsViewController.titleText = contacts[indexPath.row].name
        contactDetailsViewController.detailData = contacts[indexPath.row]
        let navController = UINavigationController(rootViewController: contactDetailsViewController)
        
        self.present(navController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
    
}
