//
//  EventDetailsViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 22/07/21.
//

import UIKit

class EventDetailsViewController: UIViewController {
    
    var titleText: String = "Belum Berubah"
    var detailData: EventDatas!
    
    let topImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 30
        imageView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        imageView.contentMode = .scaleAspectFill
        
        return imageView
    }()
    
    let descriptionViewContainer: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .white
        uiView.layer.cornerRadius = 20
        uiView.layer.shadowColor = UIColor.lightGray.cgColor
        uiView.layer.shadowOpacity = 1
        uiView.layer.shadowOffset = .zero
        uiView.layer.shadowRadius = 5
        
        return uiView
    }()
    
    let eventHistoryViewContainer: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .white
        uiView.layer.cornerRadius = 20
        uiView.layer.shadowColor = UIColor.lightGray.cgColor
        uiView.layer.shadowOpacity = 1
        uiView.layer.shadowOffset = .zero
        uiView.layer.shadowRadius = 5
        
        return uiView
    }()
    
    let moreInformationViewContainer: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .white
        uiView.layer.cornerRadius = 20
        uiView.layer.shadowColor = UIColor.lightGray.cgColor
        uiView.layer.shadowOpacity = 1
        uiView.layer.shadowOffset = .zero
        uiView.layer.shadowRadius = 5
        
        return uiView
    }()
    
    let weatherViewContainer: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .white
        uiView.layer.cornerRadius = 20
        uiView.layer.shadowColor = UIColor.lightGray.cgColor
        uiView.layer.shadowOpacity = 1
        uiView.layer.shadowOffset = .zero
        uiView.layer.shadowRadius = 5
        
        return uiView
    }()
    
    let recommendedPlanViewContainer: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .white
        uiView.layer.cornerRadius = 20
        uiView.layer.shadowColor = UIColor.lightGray.cgColor
        uiView.layer.shadowOpacity = 1
        uiView.layer.shadowOffset = .zero
        uiView.layer.shadowRadius = 5
        
        return uiView
    }()
    
    let topTitleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 20, y: 18, width: 141, height: 86))
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 30.0)
        label.numberOfLines = 0
        
        return label
    }()
    
    let topSubtitleLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 181, y: 28, width: 195, height: 20))
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.numberOfLines = 0
        
        label.textAlignment = .right
        label.text = "Placeholder"
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = self.titleText
        
        topImageView.image = UIImage(named: self.detailData.image)
        
        topTitleLabel.text = self.titleText

        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didTapCancelButton))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add plan", style: .plain, target: self, action: #selector(didTapAddToPlanButton))
        
        view.addSubview(topImageView)
        view.addSubview(descriptionViewContainer)
        view.addSubview(eventHistoryViewContainer)
        view.addSubview(moreInformationViewContainer)
        view.addSubview(weatherViewContainer)
        view.addSubview(recommendedPlanViewContainer)
        
        topImageView.addSubview(topTitleLabel)
        topImageView.addSubview(topSubtitleLabel)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let navigationControllerHeight = (navigationController?.navigationBar.frame.size.height)!
        
        topImageView.frame = CGRect(x: 0,
                                    y: navigationControllerHeight,
                                    width: view.frame.size.width,
                                    height: 150)
        
        descriptionViewContainer.frame = CGRect(x: 18,
                                                y: topImageView.frame.size.height + navigationControllerHeight + 16,
                                                width: view.frame.size.width - 36,
                                                height: 230)
        
        eventHistoryViewContainer.frame = CGRect(x: 18,
                                                 y: topImageView.frame.size.height + navigationControllerHeight + descriptionViewContainer.frame.size.height + 32,
                                                width: view.frame.size.width - 36,
                                                height: 88)
        
        moreInformationViewContainer.frame = CGRect(x: 18,
                                                    y: topImageView.frame.size.height + navigationControllerHeight + descriptionViewContainer.frame.size.height + eventHistoryViewContainer.frame.size.height + 48,
                                                    width: view.frame.size.width - 36 - 116 - 10,
                                                    height: 88)
        
        weatherViewContainer.frame = CGRect(x: moreInformationViewContainer.frame.size.width + 32,
                                            y: topImageView.frame.size.height + navigationControllerHeight + descriptionViewContainer.frame.size.height + eventHistoryViewContainer.frame.size.height + 48,
                                           width: 115,
                                           height: 88)
        
        recommendedPlanViewContainer.frame = CGRect(x: 18,
                                                    y: topImageView.frame.size.height + navigationControllerHeight + descriptionViewContainer.frame.size.height + eventHistoryViewContainer.frame.size.height + moreInformationViewContainer.frame.size.height + 64,
                                                    width: view.frame.size.width - 36,
                                                    height: 88)
    }
    
    @objc func didTapCancelButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapAddToPlanButton() {
        print("add to plann!!!!")
    }
}
