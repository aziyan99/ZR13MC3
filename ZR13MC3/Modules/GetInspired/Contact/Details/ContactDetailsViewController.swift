//
//  ContactDetailsViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 21/07/21.
//

import UIKit

class ContactDetailsViewController: UIViewController {
        
    var titleText: String = "Belum Berubah"
    var detailData: ContactDatas!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var uiViewContainer: UIView!
    @IBOutlet weak var callButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = self.titleText
        
        imageView.image = UIImage(named: detailData.image)
        imageView.layer.cornerRadius = 30
        imageView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
//        uiViewContainer.layer.borderWidth = 1
//        uiViewContainer.layer.borderColor = UIColor.lightGray.cgColor
        uiViewContainer.backgroundColor = .white
        uiViewContainer.layer.cornerRadius = 20
        uiViewContainer.layer.shadowColor = UIColor.lightGray.cgColor
        uiViewContainer.layer.shadowOpacity = 1
        uiViewContainer.layer.shadowOffset = .zero
        uiViewContainer.layer.shadowRadius = 8
        
        callButton.backgroundColor = .white
        callButton.setImage(UIImage(systemName: "phone"), for: .normal)
        callButton.layer.cornerRadius = 20
        callButton.layer.shadowColor = UIColor.lightGray.cgColor
        callButton.layer.shadowOpacity = 1
        callButton.layer.shadowOffset = .zero
        callButton.layer.shadowRadius = 8
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didTapCancelButton))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Call", style: .plain, target: self, action: #selector(didTapCallButton))
    }
    
    @objc func didTapCancelButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapCallButton() {
        print("callll!!!!")
    }
    
    @IBAction func didTapBottomCallButton(_ sender: Any) {
        print("callll!!!!")
    }
}
