//
//  MainCollectionViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 17/07/21.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {

    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    static let identifier = "MainCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 20
    }
    
    public func configure(with icon: UIImage, title: String) {
        iconView.image = icon
        iconView.tintColor = .white
        
        titleLabel.text = title
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "MainCollectionViewCell", bundle: nil)
    }
}
