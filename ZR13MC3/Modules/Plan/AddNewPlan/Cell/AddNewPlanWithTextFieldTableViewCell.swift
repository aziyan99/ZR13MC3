//
//  AddNewPlanWithTextFieldTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 21/07/21.
//

import UIKit

class AddNewPlanWithTextFieldTableViewCell: UITableViewCell {
    
    static let identifier = "UpcomingMainMenuTableViewCell"
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        
        return textField
    }()
    
    let addNewPlanLabel: UILabel = {
        let titlelabel = UILabel()
        titlelabel.text = "Name:"
        
        return titlelabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(nameTextField)
        contentView.addSubview(addNewPlanLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(){
        //
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameTextField.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addNewPlanLabel.frame = CGRect(x: 10,
                                       y: 10,
                                       width: 50,
                                       height: contentView.frame.height - 20)
        
        nameTextField.frame = CGRect(x: addNewPlanLabel.frame.size.width + 20,
                                     y: 10,
                                     width: contentView.frame.size.width - addNewPlanLabel.frame.size.width,
                                     height: contentView.frame.height - 20)
    }
    
}
