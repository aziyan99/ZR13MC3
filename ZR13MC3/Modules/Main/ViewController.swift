//
//  ViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 14/07/21.
//

import UIKit

struct CollectionViewCellMenus {
    let title: String
    let icon: String
}

class ViewController: UIViewController {
    
    @IBOutlet weak var upcomingContainerView: UIView!
    @IBOutlet weak var pastContainerView: UIView!
    @IBOutlet var mainCollectionView: UICollectionView!
    
    let menus: [CollectionViewCellMenus] = [
        CollectionViewCellMenus(title: "Lodgings", icon: "bed.double.fill"),
        CollectionViewCellMenus(title: "Attractions", icon: "gamecontroller.fill"),
        CollectionViewCellMenus(title: "Upcoming \nEvents", icon: "ticket.fill"),
        CollectionViewCellMenus(title: "Accessibility's\n", icon: "bus"),
        CollectionViewCellMenus(title: "Useful \nNumbers", icon: "phone.fill"),
        CollectionViewCellMenus(title: "Nature & \nWildlife", icon: "leaf.fill"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upcomingContainerView.isHidden = false
        pastContainerView.isHidden = true
        
        mainCollectionView.register(MainCollectionViewCell.nib(), forCellWithReuseIdentifier: MainCollectionViewCell.identifier)
        mainCollectionView.showsVerticalScrollIndicator = false
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
    }
    
    @IBAction func didTapSegment(segment: UISegmentedControl){
        // upcoming samping
        // past bawah
        if segment.selectedSegmentIndex == 0 {
            print("\(segment.selectedSegmentIndex)")
            upcomingContainerView.isHidden = false
            pastContainerView.isHidden = true
        }else{
            print("\(segment.selectedSegmentIndex)")
            upcomingContainerView.isHidden = true
            pastContainerView.isHidden = false
        }
        
    }
    
    @IBAction func didTapAddPlanButton(_ sender: Any) {
            let addNewPlanStoryboard = UIStoryboard(name: "AddNewPlan", bundle: nil)
            let addNewPlanViewController = addNewPlanStoryboard.instantiateViewController(identifier: "AddNewPlanViewController") as! AddNewPlanViewController
            navigationController?.pushViewController(addNewPlanViewController, animated: true)
    }
    
    @IBAction func didTapInfoButton(_ sender: Any) {
        print("Info button")
    }
    
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            let lodgingStoryboard = UIStoryboard(name: "Lodging", bundle: nil)
            let lodgingViewController = lodgingStoryboard.instantiateViewController(identifier: "LodgingViewController") as! LodgingViewController
            navigationController?.pushViewController(lodgingViewController, animated: true)
        }
        
        if indexPath.row == 1 {
            let attractionStoryboard = UIStoryboard(name: "Attraction", bundle: nil)
            let attractionViewController = attractionStoryboard.instantiateViewController(identifier: "AttractionViewController") as! AttractionViewController
            navigationController?.pushViewController(attractionViewController, animated: true)
        }
        
        if indexPath.row == 2 {
            let eventStoryboard = UIStoryboard(name: "Event", bundle: nil)
            let eventViewController = eventStoryboard.instantiateViewController(identifier: "EventViewController") as! EventViewController
            navigationController?.pushViewController(eventViewController, animated: true)
        }
        
        if indexPath.row == 3 {
            let accessibilityStoryboard = UIStoryboard(name: "Accessibility", bundle: nil)
            let accessibilityViewController = accessibilityStoryboard.instantiateViewController(identifier: "AccessibilityViewController") as! AccessibilityViewController
            navigationController?.pushViewController(accessibilityViewController, animated: true)
        }
        
        if indexPath.row == 4 {
            let contactStoryboard = UIStoryboard(name: "Contact", bundle: nil)
            let contactViewController = contactStoryboard.instantiateViewController(identifier: "ContactViewController") as! ContactViewController
            navigationController?.pushViewController(contactViewController, animated: true)
        }
        
        if indexPath.row == 5 {
            let natureWildlifeStoryboard = UIStoryboard(name: "NatureWildlife", bundle: nil)
            let natureWildlifeViewController = natureWildlifeStoryboard.instantiateViewController(identifier: "NatureWildlifeViewController") as! NatureWildlifeViewController
            navigationController?.pushViewController(natureWildlifeViewController, animated: true)
        }
    }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCollectionViewCell.identifier, for: indexPath) as! MainCollectionViewCell
        cell.configure(with: UIImage(systemName: menus[indexPath.row].icon)!, title: menus[indexPath.row].title)
        return cell
    }
}

