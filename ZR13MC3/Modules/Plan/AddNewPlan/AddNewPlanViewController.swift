//
//  AddNewPlanViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 17/07/21.
//

import UIKit

struct MenuDetailsWithDatePicker {
    let icon: String
    let name: String
    let date: String
    let handler: (() -> Void)
}

struct MenuDetailsWithImageAttaching {
    let icon: String
    let name: String
    let image: String
    let handler: (() -> Void)
}

struct MenuDetailsWithTextField {
    let title: String
    let handler: (() -> Void)
}

struct AddNewPlanMenus {
    let menuId: Int
    let menuDetails: [AddNewPlanOptionType]
}

enum AddNewPlanOptionType {
    case withDatePicker(model: MenuDetailsWithDatePicker)
    case withImageAttaching(model: MenuDetailsWithImageAttaching)
    case withTextField(model: MenuDetailsWithTextField)
}

class AddNewPlanViewController: UIViewController {

    let addNewPlanTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(AddNewPlanWithDatePickerTableViewCell.self, forCellReuseIdentifier: AddNewPlanWithDatePickerTableViewCell.identifier)
        
        tableView.register(AddNewPlanWithImageAttachingTableViewCell.self, forCellReuseIdentifier: AddNewPlanWithImageAttachingTableViewCell.identifier)
        
        tableView.register(AddNewPlanWithTextFieldTableViewCell.self, forCellReuseIdentifier: AddNewPlanWithTextFieldTableViewCell.identifier)
        
        return tableView
    }()
    
    var menus: [AddNewPlanMenus] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        setupNavigationBarSaveButton()
        
        addNewPlanTableView.dataSource = self
        addNewPlanTableView.delegate = self
        
        view.addSubview(addNewPlanTableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        title = "Create a plan"
        addNewPlanTableView.frame = view.bounds
    }
    
    func configure() {
        menus.append(AddNewPlanMenus(menuId: 1, menuDetails: [
            AddNewPlanOptionType.withTextField(model: MenuDetailsWithTextField(title: "", handler: {
                print("name textfield")
            }))
        ]))
        
        menus.append(AddNewPlanMenus(menuId: 2, menuDetails: [
            AddNewPlanOptionType.withDatePicker(model: MenuDetailsWithDatePicker(icon: "calendar", name: "Starts", date: "date", handler: {
                        print("date start")
                    }
            )),
            AddNewPlanOptionType.withDatePicker(model: MenuDetailsWithDatePicker(icon: "calendar", name: "Ends",  date: "date", handler: {
                        print("date end")
                    }
            ))
        ]))
        
        menus.append(AddNewPlanMenus(menuId: 3, menuDetails: [
            AddNewPlanOptionType.withImageAttaching(model: MenuDetailsWithImageAttaching(icon: "photo", name: "Cover", image: "image", handler: {
                        print("cover")
                    }
            ))
        ]))
    }
    
    func setupNavigationBarSaveButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(didTapSaveButton))
    }
    
    @objc func didTapSaveButton() {
        print("save plan")
    }
}

extension AddNewPlanViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus[section].menuDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menu = menus[indexPath.section].menuDetails[indexPath.row]
        
        switch menu.self {
        case AddNewPlanOptionType.withDatePicker(let model):
            guard let cell = addNewPlanTableView.dequeueReusableCell(withIdentifier: AddNewPlanWithDatePickerTableViewCell.identifier, for: indexPath) as? AddNewPlanWithDatePickerTableViewCell else {
                return UITableViewCell()
            }
            cell.configure(with: model)
            return cell
        case AddNewPlanOptionType.withImageAttaching(let model):
            guard let cell = addNewPlanTableView.dequeueReusableCell(withIdentifier: AddNewPlanWithImageAttachingTableViewCell.identifier, for: indexPath) as? AddNewPlanWithImageAttachingTableViewCell else {
                return UITableViewCell()
            }
            cell.configure(with: model)
            return cell
        case AddNewPlanOptionType.withTextField(_):
            guard let cell = addNewPlanTableView.dequeueReusableCell(withIdentifier: AddNewPlanWithTextFieldTableViewCell.identifier, for: indexPath) as? AddNewPlanWithTextFieldTableViewCell else {
                return UITableViewCell()
            }
            
            return cell
        }
    }
}

extension AddNewPlanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addNewPlanTableView.deselectRow(at: indexPath, animated: true)
        
        let menu = menus[indexPath.section].menuDetails[indexPath.row]
        
        switch menu.self {
        case AddNewPlanOptionType.withDatePicker(let model):
            model.handler()
        case AddNewPlanOptionType.withImageAttaching(let model):
            model.handler()
        case AddNewPlanOptionType.withTextField(model: let model):
            model.handler()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
