//
//  OnBoardingViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 16/07/21.
//

import UIKit

struct OnboardingSlide {
    let title: String
    let description: String
    let image: UIImage
}

class OnBoardingViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var letGetLostButton: UIButton!
    
    var window: UIWindow?
    
    var slides: [OnboardingSlide] = []
    
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        collectionView.showsHorizontalScrollIndicator = false
        
        letGetLostButton.isHidden = true
        slides = [
            OnboardingSlide(title: "Welcome to \nBintan Island!", description: "This app will help you to guide your extraordinary journey at bintan \nisland!", image: UIImage(named: "rect")!),
            OnboardingSlide(title: "Welcome to  \nBintan Island!", description: "This app will help you to guide your extraordinary journey at bintan \nisland!", image: UIImage(named: "rect")!),
            OnboardingSlide(title: "Welcome to  \nBintan Island!", description: "This app will help you to guide your extraordinary journey at bintan  \nisland!", image: UIImage(named: "rect")!)
        ]
    }
    
    @IBAction func didTapLetGetLostButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Loading", bundle: nil)
        let loadingController = storyboard.instantiateViewController(identifier: "LoadingNC") as! UINavigationController
        loadingController.modalPresentationStyle = .fullScreen
        loadingController.modalTransitionStyle = .coverVertical
        present(loadingController, animated: true, completion: nil)
    }
    
}

extension OnBoardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return slides.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onboardingCollectionViewCell", for: indexPath) as! OnboardingCollectionViewCell
        
        cell.setup(slides[indexPath.row])
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width  = scrollView.frame.width
        currentPage = Int(scrollView.contentOffset.x / width)
        pageControl.currentPage = currentPage
        
        if currentPage == (slides.count - 1) {
            letGetLostButton.isHidden = false
            letGetLostButton.layer.cornerRadius = 8
        }
    }
}
