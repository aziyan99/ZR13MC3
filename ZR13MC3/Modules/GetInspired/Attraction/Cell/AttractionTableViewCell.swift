//
//  AttractionTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

class AttractionTableViewCell: UITableViewCell {
    static let identifier = "attractionTableViewCell"
    
    private let attractionImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let attractionNameLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return titleLabel
    }()
    
    private let attractionAddressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        return addressLabel
    }()
    
    private let attractionTimeLabel: UILabel = {
        let timeLabel = UILabel()
        timeLabel.font = UIFont.boldSystemFont(ofSize: 12)
        return timeLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(attractionImageView)
        contentView.addSubview(attractionNameLabel)
        contentView.addSubview(attractionAddressLabel)
        contentView.addSubview(attractionTimeLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(image: String, name: String, address: String, time: String) {
        attractionImageView.image = UIImage(named: image)
        attractionNameLabel.text = name
        attractionAddressLabel.text = address
        attractionTimeLabel.text = time
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        attractionImageView.image = nil
        attractionNameLabel.text = nil
        attractionAddressLabel.text = nil
        attractionTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        attractionImageView.frame = CGRect(x: 14,
                                           y: 18,
                                           width: 85,
                                           height: contentView.frame.size.height - 36)
        
        attractionNameLabel.frame = CGRect(x: 28 + attractionImageView.frame.size.width,
                                           y: 18,
                                           width: contentView.frame.size.width - attractionImageView.frame.size.width - 10,
                                           height: 21)
                                            
        attractionAddressLabel.frame = CGRect(x: attractionImageView.frame.size.width + 28,
                                              y: attractionNameLabel.frame.height + 16,
                                              width: contentView.frame.size.width - attractionImageView.frame.size.width - 8,
                                              height: attractionNameLabel.frame.size.height + 23)
        
        attractionTimeLabel.frame = CGRect(x: attractionImageView.frame.size.width + 28,
                                           y: attractionNameLabel.frame.height + attractionAddressLabel.frame.size.height - 10,
                                           width: contentView.frame.size.width - attractionImageView.frame.size.width - 8,
                                           height: attractionNameLabel.frame.size.height + attractionAddressLabel.frame.size.height)
    }
}
