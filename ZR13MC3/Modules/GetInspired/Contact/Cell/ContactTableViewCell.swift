//
//  ContactTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    static let identifier = "contactTableViewCell"
    
    private let contactNameLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return titleLabel
    }()
    
    private let contactAddressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        return addressLabel
    }()
    
    private let contactImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let contactTimeLabel: UILabel = {
        let timeLabel = UILabel()
        timeLabel.font = UIFont.boldSystemFont(ofSize: 12)
        return timeLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(contactNameLabel)
        contentView.addSubview(contactAddressLabel)
        contentView.addSubview(contactImageView)
        contentView.addSubview(contactTimeLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(name: String, address: String, image: String, time: String){
        contactNameLabel.text = name
        contactAddressLabel.text = address
        contactImageView.image = UIImage(named: image)
        contactTimeLabel.text = time
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        contactNameLabel.text = nil
        contactAddressLabel.text = nil
        contactImageView.image = nil
        contactTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contactImageView.frame = CGRect(x: 14,
                                        y: 18,
                                        width: 85,
                                        height: contentView.frame.size.height - 36)
        
        contactNameLabel.frame = CGRect(x: 28 + contactImageView.frame.size.width,
                                         y: 18,
                                         width: contentView.frame.size.width - contactImageView.frame.size.width - 10,
                                         height: 21)
                                            
        contactAddressLabel.frame = CGRect(x: contactImageView.frame.size.width + 28,
                                           y: contactNameLabel.frame.height + 16,
                                           width: contentView.frame.size.width - contactImageView.frame.size.width - 8,
                                           height: contactNameLabel.frame.size.height + 23)
        
        contactTimeLabel.frame = CGRect(x: contactImageView.frame.size.width + 28,
                                           y: contactNameLabel.frame.height + contactAddressLabel.frame.size.height - 10,
                                           width: contentView.frame.size.width - contactImageView.frame.size.width - 8,
                                           height: contactNameLabel.frame.size.height + contactAddressLabel.frame.size.height)
    }
    
}
