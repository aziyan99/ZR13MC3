//
//  EventTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    static let identifier = "eventTableViewCell"
    
    private let eventImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let eventNameLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return titleLabel
    }()
    
    private let eventAddressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        return addressLabel
    }()
    
    private let eventTimeLabel : UILabel = {
        let timeLabel = UILabel()
        timeLabel.numberOfLines = 0
        timeLabel.font = UIFont.systemFont(ofSize: 12)
        return timeLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(eventImageView)
        contentView.addSubview(eventNameLabel)
        contentView.addSubview(eventAddressLabel)
        contentView.addSubview(eventTimeLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(image: String, name: String, address: String, time: String){
        eventImageView.image = UIImage(named: image)
        eventNameLabel.text = name
        eventAddressLabel.text = address
        eventTimeLabel.text = time
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        eventImageView.image = nil
        eventNameLabel.text = nil
        eventAddressLabel.text = nil
        eventTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        eventImageView.frame = CGRect(x: 14, y: 18, width: 85, height: contentView.frame.size.height - 36)
        
        eventNameLabel.frame = CGRect(x: 28 + eventImageView.frame.size.width, y: 18, width: contentView.frame.size.width - eventImageView.frame.size.width - 10, height: 21)
        
        eventAddressLabel.frame = CGRect(x: eventImageView.frame.size.width + 28, y: eventNameLabel.frame.height + 16, width: contentView.frame.size.width - eventImageView.frame.size.width - 8, height: eventNameLabel.frame.size.height + 23)
        
        eventTimeLabel.frame = CGRect(x: eventImageView.frame.size.width + 28, y: eventNameLabel.frame.height + eventAddressLabel.frame.size.height - 10, width: contentView.frame.size.width - eventImageView.frame.size.width - 3, height: eventNameLabel.frame.size.height + eventAddressLabel.frame.size.height)
    }
}
