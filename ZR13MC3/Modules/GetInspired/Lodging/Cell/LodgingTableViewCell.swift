//
//  LodgingTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

class LodgingTableViewCell: UITableViewCell {
    
    static let identifier = "lodgingTableViewCell"
    
    private let lodgingImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let lodgingNameLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return titleLabel
    }()
    
    private let lodgingAddressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        return addressLabel
    }()
    
    private let lodgingWebsiteLabel: UILabel = {
        let websiteLabel = UILabel()
        websiteLabel.font = UIFont.boldSystemFont(ofSize: 12)
        return websiteLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(lodgingImageView)
        contentView.addSubview(lodgingNameLabel)
        contentView.addSubview(lodgingAddressLabel)
        contentView.addSubview(lodgingWebsiteLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(image: String, name: String, address: String, website: String) {
        lodgingImageView.image = UIImage(named: image)
        lodgingNameLabel.text = name
        lodgingAddressLabel.text = address
        lodgingWebsiteLabel.text = website
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lodgingImageView.image = nil
        lodgingNameLabel.text = nil
        lodgingAddressLabel.text = nil
        lodgingWebsiteLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        lodgingImageView.frame = CGRect(x: 14,
                                        y: 18,
                                        width: 85,
                                        height: contentView.frame.size.height - 36)
        
        lodgingNameLabel.frame = CGRect(x: 28 + lodgingImageView.frame.size.width,
                                         y: 18,
                                         width: contentView.frame.size.width - lodgingImageView.frame.size.width - 10,
                                         height: 21)
                                            
        lodgingAddressLabel.frame = CGRect(x: lodgingImageView.frame.size.width + 28,
                                           y: lodgingNameLabel.frame.height + 16,
                                           width: contentView.frame.size.width - lodgingImageView.frame.size.width - 8,
                                           height: lodgingNameLabel.frame.size.height + 23)
        
        lodgingWebsiteLabel.frame = CGRect(x: lodgingImageView.frame.size.width + 28,
                                           y: lodgingNameLabel.frame.height + lodgingAddressLabel.frame.size.height - 10,
                                           width: contentView.frame.size.width - lodgingImageView.frame.size.width - 8,
                                           height: lodgingNameLabel.frame.size.height + lodgingAddressLabel.frame.size.height)
    }
}
