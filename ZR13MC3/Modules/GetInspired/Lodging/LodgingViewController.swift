//
//  LodgingViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

struct LodgingDatas {
    let image: String
    let name: String
    let address: String
    let website: String
}

class LodgingViewController: UIViewController {
    
    let lodgings: [LodgingDatas] = [
        LodgingDatas(image: "rect", name: "Resort A", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resorta.com"),
        LodgingDatas(image: "rect", name: "Resort B", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resortb.com"),
        LodgingDatas(image: "rect", name: "Resort C", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resortc.com"),
        LodgingDatas(image: "rect", name: "Resort D", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resortd.com"),
        LodgingDatas(image: "rect", name: "Resort E", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resorte.com"),
        LodgingDatas(image: "rect", name: "Resort F", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resortf.com"),
        LodgingDatas(image: "rect", name: "Resort G", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resortg.com"),
        LodgingDatas(image: "rect", name: "Resort H", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resorth.com"),
        LodgingDatas(image: "rect", name: "Resort I", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resorti.com"),
        LodgingDatas(image: "rect", name: "Resort J", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", website: "resortj.com"),
    ]
    
    private let lodgingTableView: UITableView =  {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(LodgingTableViewCell.self,
                           forCellReuseIdentifier: LodgingTableViewCell.identifier)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Lodgings"
            
        lodgingTableView.dataSource = self
        lodgingTableView.delegate = self
        
        view.addSubview(lodgingTableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        lodgingTableView.frame = view.bounds
    }
}

extension LodgingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lodgings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = lodgingTableView.dequeueReusableCell(withIdentifier: LodgingTableViewCell.identifier, for: indexPath) as? LodgingTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(image: lodgings[indexPath.row].image,
                        name: lodgings[indexPath.row].name,
                        address: lodgings[indexPath.row].address,
                        website: lodgings[indexPath.row].website)
        
        return cell
    }
}

extension LodgingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lodgingTableView.deselectRow(at: indexPath, animated: true)
        
        print("you select \(lodgings[indexPath.row].name)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
}
