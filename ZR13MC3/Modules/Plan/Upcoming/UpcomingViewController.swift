//
//  UpcomingViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 16/07/21.
//

import UIKit

class UpcomingViewController: UIViewController {

    let addNewPlanButton: UIButton = UIButton(frame: CGRect(x: 16,
                                                  y: 8,
                                                  width: 355,
                                                  height: 120))
    let icon: UIImage = (UIImage(systemName: "plus.circle")?.withTintColor(.white, renderingMode: .alwaysOriginal))!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAddNewPlanButton()
    }
    
    func setupAddNewPlanButton() {
        addNewPlanButton.setTitle(" Let's add a new plan", for: .normal)
        addNewPlanButton.setImage(icon, for: .normal)
        addNewPlanButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.bold)
        addNewPlanButton.setTitleColor(.white, for: .normal)
        addNewPlanButton.backgroundColor = .lightGray
        addNewPlanButton.layer.cornerRadius = 20
        addNewPlanButton.addTarget(self, action: #selector(didTapAddNewPlanButton), for: .touchUpInside)
        
        self.view.addSubview(addNewPlanButton)
    }
    
    @objc func didTapAddNewPlanButton() {
        let addNewPlanStoryboard = UIStoryboard(name: "AddNewPlan", bundle: nil)
        let addNewPlanViewController = addNewPlanStoryboard.instantiateViewController(identifier: "AddNewPlanViewController") as! AddNewPlanViewController
        navigationController?.pushViewController(addNewPlanViewController, animated: true)
    }
}
