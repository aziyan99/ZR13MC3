//
//  AttractionViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

struct AttractionDatas {
    let image: String
    let name: String
    let address: String
    let time: String
}

class AttractionViewController: UIViewController {
    
    let attractions: [AttractionDatas] = [
        AttractionDatas(image: "rect", name: "Pantai Trikora A", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora B", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora C", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora D", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora E", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora F", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora G", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora H", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora I", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
        AttractionDatas(image: "rect", name: "Pantai Trikora J", address: "Pulau Bintan, Kepulauan Riau, \nIndonesia.", time: "09.00 - 17.00"),
    ]

    private let attractionTableView: UITableView =  {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(AttractionTableViewCell.self,
                           forCellReuseIdentifier: AttractionTableViewCell.identifier)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Attractions"
        
        attractionTableView.dataSource = self
        attractionTableView.delegate = self
        
        view.addSubview(attractionTableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        attractionTableView.frame = view.bounds
    }
}

extension AttractionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attractions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = attractionTableView.dequeueReusableCell(withIdentifier: AttractionTableViewCell.identifier, for: indexPath) as? AttractionTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure( image: attractions[indexPath.row].image,
                        name: attractions[indexPath.row].name,
                        address: attractions[indexPath.row].address,
                        time: attractions[indexPath.row].time)
        
        return cell
    }
}

extension AttractionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        attractionTableView.deselectRow(at: indexPath, animated: true)
        
        print("you select \(attractions[indexPath.row].name)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
}

