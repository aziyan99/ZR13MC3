//
//  AddNewPlanWithDatePickerTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 19/07/21.
//

import UIKit

class AddNewPlanWithDatePickerTableViewCell: UITableViewCell {

    static let identifier = "AddNewPlanWithDatePickerTableViewCell"
    
    let addNewPlanImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = .black
        return imageView
    }()
    
    let addNewPlanLabel: UILabel = {
        let titlelabel = UILabel()
        titlelabel.font = UIFont.systemFont(ofSize: 12)
        
        return titlelabel
    }()
    
    let addNewPlanDatePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.timeZone = NSTimeZone.local
        
        return datePicker
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(addNewPlanImageView)
        contentView.addSubview(addNewPlanLabel)
        contentView.addSubview(addNewPlanDatePicker)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(with menu: MenuDetailsWithDatePicker) {
        addNewPlanImageView.image = UIImage(systemName: menu.icon)
        addNewPlanLabel.text = menu.name
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        addNewPlanImageView.image = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addNewPlanImageView.frame = CGRect(x: 20,
                                           y: 10,
                                           width: 70,
                                           height: contentView.frame.height - 20)
        
        addNewPlanLabel.frame = CGRect(x: addNewPlanImageView.frame.width + 30,
                                       y: 8,
                                       width: contentView.frame.width - 10,
                                       height: contentView.frame.height - 45)
        
        addNewPlanDatePicker.frame = CGRect(x: addNewPlanImageView.frame.width + 30,
                                            y: addNewPlanLabel.frame.size.height,
                                            width: contentView.frame.width - 10,
                                            height: contentView.frame.height - 50)
    }

}
