//
//  AccessibilityViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

struct AccessibilityDatas {
    let name: String
    let image: String
    let address: String
    let time: String
}

class AccessibilityViewController: UIViewController {
    
    let accessibilities: [AccessibilityDatas] = [
        AccessibilityDatas(name: "satu", image: "rect", address: "dihatimu", time: "09.00 - 17.00"),
        AccessibilityDatas(name: "dua", image: "rect", address: "dihatimu", time: "09.00 - 17.00"),
        AccessibilityDatas(name: "tiga", image: "rect", address: "dihatimu", time: "09.00 - 17.00"),
        AccessibilityDatas(name: "empat", image: "rect", address: "dihatimu", time: "09.00 - 17.00"),
        AccessibilityDatas(name: "lima", image: "rect", address: "dihatimu", time: "09.00 - 17.00"),
    ]
    
    private let accessibilityTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .insetGrouped)
        tableView.register(AccessibilityTableViewCell.self, forCellReuseIdentifier: AccessibilityTableViewCell.identifier)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Accessibiliti's"
        accessibilityTableView.delegate = self
        accessibilityTableView.dataSource = self
        view.addSubview(accessibilityTableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        accessibilityTableView.frame = view.bounds
    }
}

extension AccessibilityViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        accessibilities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = accessibilityTableView.dequeueReusableCell(withIdentifier: AccessibilityTableViewCell.identifier, for: indexPath) as? AccessibilityTableViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(image: accessibilities[indexPath.row].image, name: accessibilities[indexPath.row].name, address: accessibilities[indexPath.row].address, time: accessibilities[indexPath.row].time)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        accessibilityTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 117
    }
}
