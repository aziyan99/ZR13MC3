//
//  LoadingViewController.swift
//  ZR13MC3
//
//  Created by Raja Azian on 17/07/21.
//

import UIKit

struct LoadingSlides {
    let title: String
    let description: String
    let image: UIImage
}

class LoadingViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var progressBarView: UIProgressView!
    
    let progress = Progress(totalUnitCount: 3)
    private var countInit = 0
    
    let slides: [LoadingSlides] = [
        LoadingSlides(title: "Bintan Trip \nTips & Tricks", description: "This app will help you to guide your extraordinary journey at bintan island!", image: UIImage(named: "rect")!),
        LoadingSlides(title: "Traveling \nLifehacks", description: "This app will help you to guide your extraordinary journey at bintan island!", image: UIImage(named: "rect")!),
        LoadingSlides(title: "Bintan \nFun Facts", description: "This app will help you to guide your extraordinary journey at bintan island!", image: UIImage(named: "rect")!)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.titleLabel.text = self.slides[0].title
        self.descriptionLabel.text = self.slides[0].description
        self.imageView.image = self.slides[0].image
        
        self.titleLabel.numberOfLines = 0
        self.descriptionLabel.numberOfLines = 0
        
        //1
        progressBarView.progress = 0.0
        progress.completedUnitCount = 0
         //2
        Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
            guard self.progress.isFinished == false else {
                timer.invalidate()
                return
            }
            
            self.titleLabel.text = self.slides[self.countInit].title
            self.descriptionLabel.text = self.slides[self.countInit].description
            self.imageView.image = self.slides[self.countInit].image
                
            // 3
            self.progress.completedUnitCount += 1
            self.progressBarView.setProgress(Float(self.progress.fractionCompleted), animated: true)
            self.countInit += 1
            self.getCountInit()
        }
    }
    
    func getCountInit() {
        if self.countInit == 3 {
                Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { (timer) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainViewController = storyboard.instantiateViewController(identifier: "MainNC") as! UINavigationController
                mainViewController.modalPresentationStyle = .fullScreen
                mainViewController.modalTransitionStyle = .coverVertical
                self.present(mainViewController, animated: true, completion: nil)
            }
        }
    }
}

