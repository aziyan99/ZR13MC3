//
//  AccessibilityTableViewCell.swift
//  ZR13MC3
//
//  Created by Raja Azian on 18/07/21.
//

import UIKit

class AccessibilityTableViewCell: UITableViewCell {
    static let identifier = "accessibilityTableViewCell"
    
    private let accessibilityImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    private let accessibilityNameLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        return titleLabel
    }()
    
    private let accessibilityAddressLabel: UILabel = {
        let addressLabel = UILabel()
        addressLabel.numberOfLines = 0
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        return addressLabel
    }()
    
    private let accessibilityTimeLabel: UILabel = {
        let timeLabel = UILabel()
        timeLabel.numberOfLines = 0
        timeLabel.font = UIFont.systemFont(ofSize: 12)
        return timeLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(accessibilityImageView)
        contentView.addSubview(accessibilityNameLabel)
        contentView.addSubview(accessibilityAddressLabel)
        contentView.addSubview(accessibilityTimeLabel)
    }
    
    required init?(coder: NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(image: String, name: String, address: String, time: String){
        accessibilityImageView.image = UIImage(named: image)
        accessibilityNameLabel.text = name
        accessibilityAddressLabel.text = address
        accessibilityTimeLabel.text = time
    }
    
    override func prepareForReuse(){
        super.prepareForReuse()
        accessibilityImageView.image = nil
        accessibilityNameLabel.text = nil
        accessibilityAddressLabel.text = nil
        accessibilityTimeLabel.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        accessibilityImageView.frame = CGRect(x: 14,
                                        y: 18,
                                        width: 85,
                                        height: contentView.frame.size.height - 36)
        
        accessibilityNameLabel.frame = CGRect(x: 28 + accessibilityImageView.frame.size.width,
                                         y: 18,
                                         width: contentView.frame.size.width - accessibilityImageView.frame.size.width - 10,
                                         height: 21)
                                            
        accessibilityAddressLabel.frame = CGRect(x: accessibilityImageView.frame.size.width + 28,
                                           y: accessibilityNameLabel.frame.height + 16,
                                           width: contentView.frame.size.width - accessibilityImageView.frame.size.width - 8,
                                           height: accessibilityNameLabel.frame.size.height + 23)
        
        accessibilityTimeLabel.frame = CGRect(x: accessibilityImageView.frame.size.width + 28,
                                           y: accessibilityNameLabel.frame.height + accessibilityAddressLabel.frame.size.height - 10,
                                           width: contentView.frame.size.width - accessibilityImageView.frame.size.width - 8,
                                           height: accessibilityNameLabel.frame.size.height + accessibilityAddressLabel.frame.size.height)
    }
}
